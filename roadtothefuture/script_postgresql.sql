create SCHEMA IF NOT EXISTS phone_manager;

CREATE TABLE IF NOT EXISTS phone_manager.app
(
  id          SERIAL PRIMARY KEY,
  name        varchar(50) UNIQUE NOT NULL,
  description varchar(50)
);

CREATE TABLE IF NOT EXISTS phone_manager.action
(
  id          SERIAL PRIMARY KEY,
  name        varchar(50) NOT NULL,
  description varchar(50),
  app_id      integer,
  FOREIGN KEY (app_id) REFERENCES phone_manager.app (id)
);

CREATE TABLE IF NOT EXISTS phone_manager.dump
(
  id          SERIAL PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS phone_manager.phone_status
(
  id          SERIAL PRIMARY KEY,
  name        varchar(50) UNIQUE NOT NULL,
  description varchar(50)
);

CREATE TABLE IF NOT EXISTS phone_manager.raspberrypi_status
(
  id          SERIAL PRIMARY KEY,
  name        varchar(50) UNIQUE NOT NULL,
  description varchar(50)
);

CREATE TABLE IF NOT EXISTS phone_manager.raspberrypi
(
  id          SERIAL PRIMARY KEY,
  ip          varchar(50),
  status_id   integer,
  FOREIGN KEY (status_id) REFERENCES phone_manager.raspberrypi_status (id)
);


CREATE TABLE IF NOT EXISTS phone_manager.phone
(
  id          SERIAL PRIMARY KEY,
  name        varchar(50) UNIQUE NOT NULL,
  proxy       varchar(15),
  dump_id     integer,
  status_id   integer,
  raspberrypi_id integer,
  FOREIGN KEY (dump_id) REFERENCES phone_manager.dump (id),
  FOREIGN KEY (status_id) REFERENCES phone_manager.phone_status (id),
  FOREIGN KEY (raspberrypi_id) REFERENCES phone_manager.raspberrypi (id)
);

CREATE TABLE IF NOT EXISTS phone_manager.task_status
(
  id          SERIAL PRIMARY KEY,
  name        varchar(50) UNIQUE NOT NULL,
  description varchar(50)
);

CREATE TABLE IF NOT EXISTS phone_manager.task
(
  id          SERIAL PRIMARY KEY,
  action_id   integer,
  status_id   integer,
  phone_id    integer,
  payload     jsonb,
  FOREIGN KEY (action_id) REFERENCES phone_manager.action (id),
  FOREIGN KEY (status_id) REFERENCES phone_manager.task_status (id),
  FOREIGN KEY (phone_id) REFERENCES phone_manager.phone (id)
);



-- ----------------------------------------INSERT PHONE STATUS DATA-----------------------------------------------------
INSERT INTO phone_manager.phone_status(name, description)
values ('FREE','Free phone');

INSERT INTO phone_manager.phone_status(name, description)
values ('BUSY','Busy phone');

-- ----------------------------------------INSERT TASK STATUS DATA------------------------------------------------------
INSERT INTO phone_manager.task_status(name, description)
values ('NOT_STARTED','Task not started');

INSERT INTO phone_manager.task_status(name, description)
values ('IN_PROGRESS','Task in progress');

INSERT INTO phone_manager.task_status(name, description)
values ('SUCCESS','Success task');

INSERT INTO phone_manager.task_status(name, description)
values ('ERROR','Error task');

-- ----------------------------------------INSERT RASPBERRY PI STATUS DATA----------------------------------------------

INSERT INTO phone_manager.raspberrypi_status(name, description)
values ('OFFLINE', 'Raspberry pi offline');

INSERT INTO phone_manager.raspberrypi_status(name, description)
values ('ONLINE', 'Raspberry pi online');

-- ----------------------------------------INSERT APP DATA--------------------------------------------------------------
INSERT INTO phone_manager.app(name, description)
values ('Instagram', 'Instagram application');

INSERT INTO phone_manager.app(name, description)
values ('Youtube', 'Youtube application');

INSERT INTO phone_manager.app(name, description)
values ('Phone', 'Youtube application');

-- ----------------------------------------INSERT TASK DATA-------------------------------------------------------------
INSERT INTO phone_manager.action(name, description, app_id)
values ('Like', 'Like action for Instagram', 1);

INSERT INTO phone_manager.action(name, description, app_id)
values ('Follow', 'Follow action for Instagram', 1);

INSERT INTO phone_manager.action(name, description, app_id)
values ('Like', 'Like action for Youtube', 2);

INSERT INTO phone_manager.action(name, description, app_id)
values ('Wipe', 'Wipe phone', 3);

INSERT INTO phone_manager.action(name, description, app_id)
values ('Wifi_proxy', 'Setup wifi and proxy', 3);

INSERT INTO phone_manager.action(name, description, app_id)
values ('Install_instagram', 'Install Instagram on phone', 3);

INSERT INTO phone_manager.action(name, description, app_id)
values ('Login', 'Login action for Instagram', 1);

INSERT INTO phone_manager.action(name, description, app_id)
values ('Registration', 'Registration action for Instagram', 1);

-- ----------------------------------------INSERT RASPBERRY PI DATA-----------------------------------------------------
INSERT INTO phone_manager.raspberrypi(ip, status_id)
values ('127.0.0.1', 2);

-- ----------------------------------------INSERT PHONE DATA------------------------------------------------------------
INSERT INTO phone_manager.phone(name, status_id, raspberrypi_id)
values ('Test phone', 1, 1);

CREATE SCHEMA IF NOT EXISTS user_data;

CREATE TABLE IF NOT EXISTS user_data.domains
(
  id          SERIAL PRIMARY KEY,
  domain        varchar(100) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS user_data.insta
(
  id          SERIAL PRIMARY KEY,
  email       varchar(100) UNIQUE NOT NULL,
  login       varchar(50),
  name        varchar(100) UNIQUE NOT NULL,
  password    varchar(50) NOT NULL
);