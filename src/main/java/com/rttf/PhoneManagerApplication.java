package com.rttf;

import com.rttf.phonemanager.business.google.parser.CommonGoogleParser;
import com.rttf.phonemanager.business.helper.Bounds;
import com.rttf.phonemanager.business.helper.JadbDeviceHelper;
import com.rttf.phonemanager.business.helper.KeyEvent;
import com.rttf.phonemanager.business.instagram.parser.CommonInstagramParser;
import com.rttf.phonemanager.business.phone.parser.CommonPhoneParser;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import se.vidstige.jadb.JadbConnection;
import se.vidstige.jadb.JadbDevice;
import se.vidstige.jadb.RemoteFile;

import java.io.File;

@SpringBootApplication
@EnableScheduling
public class PhoneManagerApplication {

    @SneakyThrows
    public static void main(String[] args) {
//        SpringApplication.run(PhoneManagerApplication.class, args);
        CommonPhoneParser commonPhoneParser = new CommonPhoneParser();
        CommonInstagramParser commonInstagramParser = new CommonInstagramParser();
        CommonGoogleParser commonGoogleParser = new CommonGoogleParser();
        JadbConnection jadbConnection = new JadbConnection("127.0.0.1", 5037);
        JadbDevice jadbDevice = jadbConnection.getDevices().get(0);
        JadbDeviceHelper jadbDeviceHelper = new JadbDeviceHelper(jadbDevice);
//        jadbDeviceHelper.unlock();
//        Thread.sleep(1000);
//        jadbDeviceHelper.startApp("com.android.vending");
//        Thread.sleep(20000);
//        jadbDeviceHelper.tap(commonGoogleParser.getCreateAccountButtonCoordinates(jadbDeviceHelper.dump()));
//        Thread.sleep(1000);
//        jadbDeviceHelper.tap(commonGoogleParser.getForMyselfCoordinates(jadbDeviceHelper.dump()));
//        Thread.sleep(20000);
//        jadbDeviceHelper.tap(commonPhoneParser.getButtonOk(jadbDeviceHelper.dump()));
//        Thread.sleep(1000);
//        jadbDeviceHelper.text("Elena");
//        Thread.sleep(4000);
//        jadbDeviceHelper.tap(commonGoogleParser.getLastNameCoordinates(jadbDeviceHelper.dump()));
//        Thread.sleep(2000);
//        jadbDeviceHelper.text("Wander");
//        Thread.sleep(1000);
//        jadbDeviceHelper.keyEvent(KeyEvent.KEYCODE_BACK);
//        Thread.sleep(4000);
//        jadbDeviceHelper.tap(commonGoogleParser.getCollectNamesNextCoordinates(jadbDeviceHelper.dump()));
//        Thread.sleep(10000);
//        String birthdatScreenDump = jadbDeviceHelper.dump();
//        jadbDeviceHelper.dump();

        jadbDeviceHelper.text(" https://af.vkplus.me/yb");
        Thread.sleep(2000);
        jadbDeviceHelper.text("/oauth.php?clientId");
        Thread.sleep(2000);
        jadbDeviceHelper.text("=315013817583-c5v");
        Thread.sleep(2000);
        jadbDeviceHelper.text("b74a4h8vi1odue70");
        Thread.sleep(2000);
        jadbDeviceHelper.text("gspo0o2k33tsf.apps");
        Thread.sleep(2000);
        jadbDeviceHelper.text(".googleusercon");
        Thread.sleep(2000);
        jadbDeviceHelper.text("tent.com&secret=ON");
        Thread.sleep(2000);
        jadbDeviceHelper.text("1YakwADQRS_");
        Thread.sleep(2000);
        jadbDeviceHelper.text("BoY4h1Xw8XF");
        Thread.sleep(2000);
//        jadbDeviceHelper.text("https://console.developers.google.com/");
//        jadbDeviceHelper.text("https://af.vkplus.me/yb/oauth2callback.php");
//        jadbDeviceHelper.text("https://af.vkplus.me/yb/oauth.php");
//        Thread.sleep(10000);
//        jadbDeviceHelper.text("?clientId=495116536702");
//        jadbDeviceHelper.text("-j8g8devr5uh4clrdvkkubgck7krosbl4.apps.googleusercontent.com&secret=BVhrQo1P49fJzl2QvG5h_IIT");

//        jadbDeviceHelper.text(".apps.googleusercontent.com&secret=BVhrQo1P49fJzl2QvG5h_IIT");
//        Thread.sleep(10000);

//        jadbDeviceHelper.text("https://af.vkplus.me/yb/");
//        Thread.sleep(5000);
//        jadbDeviceHelper.text("oauth.php?clientId=49511653");
//        Thread.sleep(5000);
//        jadbDeviceHelper.text("6702-j8g8devr5uh4clrdvkkubgc");
//        Thread.sleep(5000);
//        jadbDeviceHelper.text("k7krosbl4.apps.googleuse");
//        Thread.sleep(5000);
//        jadbDeviceHelper.text("rcontent.com&secret=B");
//        Thread.sleep(5000);
//        jadbDeviceHelper.text("VhrQo1P49fJzl2QvG5h_IIT");
//        Thread.sleep(5000);
//        jadbDeviceHelper.unlock();


//        SpringApplication.run(PhoneManagerApplication.class, args);
    }

}
