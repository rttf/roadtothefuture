package com.rttf.rest.model.response;

import lombok.Data;

@Data
public class PhoneStatusResponse {
    private Long id;
    private String name;
    private String description;
}
