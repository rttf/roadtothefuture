package com.rttf.rest.model.response;

import com.rttf.business.model.Action;
import com.rttf.business.model.Phone;
import com.rttf.business.model.TaskStatus;
import lombok.Data;

import java.util.Map;

@Data
public class TaskResponse {
    private Long id;
    private Action action;
    private String status;
    private Phone phone;
    private Map<String, String> payload;
}
