package com.rttf.rest.model.response;

import com.rttf.business.model.RaspberryPi;
import lombok.Data;

@Data
public class PhoneResponse {
    private Long id;
    private String name;
    private String proxy;
    private String status;
    private Long dumpId;
    private RaspberryPi raspberryPi;
}
