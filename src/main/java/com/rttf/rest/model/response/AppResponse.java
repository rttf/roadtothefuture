package com.rttf.rest.model.response;

import lombok.Data;

@Data
public class AppResponse {
    private Long id;
    private String name;
    private String description;
}
