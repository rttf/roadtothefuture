package com.rttf.rest.model.response;

import com.rttf.transactional.entity.app.AppEntity;
import lombok.Data;

@Data
public class ActionResponse {
    private Long id;
    private String name;
    private String description;
    private AppEntity app;
}
