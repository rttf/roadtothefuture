package com.rttf.rest.model.request.task;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Data
public class CommonTaskRequest {

    @Null
    private Long id;

    @NotNull
    private Long phoneId;

}
