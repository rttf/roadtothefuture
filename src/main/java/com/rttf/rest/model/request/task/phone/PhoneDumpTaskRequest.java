package com.rttf.rest.model.request.task.phone;

import com.rttf.rest.model.request.task.CommonTaskRequest;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class PhoneDumpTaskRequest extends CommonTaskRequest {

    @NotEmpty
    private String dump;

}
