package com.rttf.rest.model.request.task.phone;

import com.rttf.rest.model.request.task.CommonTaskRequest;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class PhoneWifiTaskRequest extends CommonTaskRequest {

    @NotEmpty
    private String name;

    @NotEmpty
    private String password;

    private String proxyIp;

    private String proxyPort;

}
