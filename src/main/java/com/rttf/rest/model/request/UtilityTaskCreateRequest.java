package com.rttf.rest.model.request;

import com.rttf.business.model.TaskPayload;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;

@Data
public class UtilityTaskCreateRequest implements Serializable {

    @Null
    private Long id;

    @NotNull
    private Long actionId;

    @NotNull
    private Long phoneId;

    @NotNull
    private TaskPayload payload;

}
