package com.rttf.rest.model.request.task.instagram;

import com.rttf.rest.model.request.task.CommonTaskRequest;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class InstagramFollowTaskRequest extends CommonTaskRequest {

    @NotEmpty
    private String profile;

}
