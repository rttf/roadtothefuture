package com.rttf.rest.model.request.task.instagram;

import com.rttf.rest.model.request.task.CommonTaskRequest;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class InstagramRegistrationTaskRequest extends CommonTaskRequest {

    @NotEmpty
    private String login;
    @NotEmpty
    private String password;
    @NotEmpty
    private String name;
}
