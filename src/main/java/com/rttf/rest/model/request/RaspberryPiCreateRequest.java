package com.rttf.rest.model.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Null;

@Data
//TODO Add swagger
public class RaspberryPiCreateRequest {

    @Null
    private Long id;
    @NotEmpty
    private String ip;

}
