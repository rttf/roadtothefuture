package com.rttf.rest.controller;

import com.rttf.business.model.RaspberryPi;
import com.rttf.business.service.raspberrypi.RaspberryPiService;
import com.rttf.rest.model.request.RaspberryPiCreateRequest;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.rttf.config.swagger.SwaggerConfiguration.RASPBERRY_PI_TAG;

@RestController
@RequestMapping("/v1/raspberrypi")
@AllArgsConstructor
@Api(tags = RASPBERRY_PI_TAG)
public class RaspberryPiRestControllerV1 {

    private MapperFacade mapper;

    private RaspberryPiService raspberryPiService;

    @GetMapping("ping")
    public void ping() {
    }

    @PostMapping
    public RaspberryPi addRaspberryPi(@RequestBody RaspberryPiCreateRequest raspberryPiCreateRequest) {
        //TODO add mapping to response
        return raspberryPiService.saveRaspberryPi(mapper.map(raspberryPiCreateRequest, RaspberryPi.class));
    }

}
