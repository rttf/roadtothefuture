package com.rttf.rest.controller;


import com.rttf.business.service.phone.PhoneStatusService;
import com.rttf.rest.model.response.PhoneStatusResponse;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.stream.Collectors;

import static com.rttf.config.swagger.SwaggerConfiguration.PHONE_STATUS_TAG;

@RestController
@RequestMapping("/v1/phone/status")
@AllArgsConstructor
@Api(tags = PHONE_STATUS_TAG)
public class PhoneStatusRestControllerV1 {

    private static final String ID = "id";

    private final MapperFacade mapper;

    private final PhoneStatusService phoneStatusService;

    @GetMapping("/{id}")
    public PhoneStatusResponse findById(@PathVariable(value = ID) Long phoneStatusId) {
        return mapper.map(phoneStatusService.findById(phoneStatusId), PhoneStatusResponse.class);
    }

    @GetMapping
    public Collection<PhoneStatusResponse> findAll() {
        return phoneStatusService.findAll().stream().map(app -> mapper.map(app, PhoneStatusResponse.class)).collect(Collectors.toList());
    }

}
