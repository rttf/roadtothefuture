package com.rttf.rest.controller;

import com.rttf.business.service.action.ActionService;
import com.rttf.rest.model.response.ActionResponse;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.stream.Collectors;

import static com.rttf.config.swagger.SwaggerConfiguration.ACTION_STATUS_TAG;

@RestController
@RequestMapping("/v1/action")
@AllArgsConstructor
@Api(tags = ACTION_STATUS_TAG)
public class ActionRestControllerV1 {

    private static final String ID = "id";

    private final MapperFacade mapper;

    private final ActionService actionService;

    @GetMapping("/{id}")
    public ActionResponse findById(@PathVariable(value = ID) Long actionId) {
        return mapper.map(actionService.findById(actionId), ActionResponse.class);
    }

    @GetMapping
    public Collection<ActionResponse> findAll() {
        return actionService.findAll().stream().map(app -> mapper.map(app, ActionResponse.class)).collect(Collectors.toList());
    }
}