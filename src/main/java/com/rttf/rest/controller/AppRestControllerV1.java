package com.rttf.rest.controller;

import com.rttf.business.service.app.AppService;
import com.rttf.rest.model.response.AppResponse;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.stream.Collectors;

import static com.rttf.config.swagger.SwaggerConfiguration.APP_TAG;

@RestController
@RequestMapping("/v1/app")
@AllArgsConstructor
@Api(tags = APP_TAG)
public class AppRestControllerV1 {

    private static final String ID = "id";

    private final MapperFacade mapper;

    private final AppService appService;

    @GetMapping("/{id}")
    public AppResponse findById(@PathVariable(value = ID) Long appId) {
        return mapper.map(appService.findById(appId), AppResponse.class);
    }

    @GetMapping
    public Collection<AppResponse> findAll() {
        return appService.findAll().stream().map(app -> mapper.map(app, AppResponse.class)).collect(Collectors.toList());
    }
}
