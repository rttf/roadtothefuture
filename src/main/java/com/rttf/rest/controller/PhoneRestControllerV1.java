package com.rttf.rest.controller;


import com.rttf.business.service.phone.PhoneService;
import com.rttf.rest.model.response.PhoneResponse;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.stream.Collectors;

import static com.rttf.config.swagger.SwaggerConfiguration.PHONE_TAG;

@RestController
@RequestMapping("/v1/phone")
@AllArgsConstructor
@Api(tags = PHONE_TAG)
public class PhoneRestControllerV1 {

    private static final String ID = "id";

    private final MapperFacade mapper;

    private final PhoneService phoneService;

    @GetMapping("/{id}")
    public PhoneResponse findById(@PathVariable(value = ID) Long phoneId) {
        return mapper.map(phoneService.findById(phoneId), PhoneResponse.class);
    }

    @GetMapping
    public Collection<PhoneResponse> findAll() {
        return phoneService.findAll().stream().map(app -> mapper.map(app, PhoneResponse.class)).collect(Collectors.toList());
    }
}
