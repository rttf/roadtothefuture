package com.rttf.rest.controller;

import com.rttf.business.service.task.TaskStatusService;
import com.rttf.rest.model.response.TaskStatusResponse;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.stream.Collectors;

import static com.rttf.config.swagger.SwaggerConfiguration.TASK_STATUS_TAG;

@RestController
@RequestMapping("/v1/task/status")
@AllArgsConstructor
@Api(tags = TASK_STATUS_TAG)
public class TaskStatusRestControllerV1 {

    private static final String ID = "id";

    private final MapperFacade mapper;

    private final TaskStatusService taskStatusService;

    @GetMapping("/{id}")
    public TaskStatusResponse findById(@PathVariable(value = ID) Long taskStatusId) {
        return mapper.map(taskStatusService.findById(taskStatusId), TaskStatusResponse.class);
    }

    @GetMapping
    public Collection<TaskStatusResponse> findAll() {
        return taskStatusService.findAll().stream().map(taskStatus -> mapper.map(taskStatus, TaskStatusResponse.class)).collect(Collectors.toList());
    }

}
