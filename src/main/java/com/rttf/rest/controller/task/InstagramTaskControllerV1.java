package com.rttf.rest.controller.task;

import com.rttf.business.model.Action;
import com.rttf.business.model.Task;
import com.rttf.business.service.task.TaskService;
import com.rttf.rest.model.request.task.instagram.InstagramFollowTaskRequest;
import com.rttf.rest.model.request.task.instagram.InstagramLikeTaskRequest;
import com.rttf.rest.model.request.task.instagram.InstagramLoginTaskRequest;
import com.rttf.rest.model.request.task.instagram.InstagramRegistrationTaskRequest;
import com.rttf.rest.model.response.TaskResponse;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.rttf.config.swagger.SwaggerConfiguration.INSTAGRAM_TASK_TAG;

@RestController
@RequestMapping("/v1/task/instagram")
@AllArgsConstructor
@Api(tags = INSTAGRAM_TASK_TAG)
public class InstagramTaskControllerV1 {

    private MapperFacade mapper;

    private TaskService taskService;

    @PostMapping("/like")
    public TaskResponse like(@RequestBody InstagramLikeTaskRequest instagramLikeTaskRequest) {
        //TODO refactor
        Task task = mapper.map(instagramLikeTaskRequest, Task.class);
        Action action = new Action();
        //TODO GET ACTION  and all dictionary value FROM DYNAMIC ENUM(enum get values from database)
        action.setId(1L);
        task.setAction(action);
        return mapper.map(taskService.save(task), TaskResponse.class);
    }

    @PostMapping("/follow")
    public TaskResponse follow(@RequestBody InstagramFollowTaskRequest instagramFollowTaskRequest) {
        Task task = mapper.map(instagramFollowTaskRequest, Task.class);
        Action action = new Action();
        action.setId(2L);
        task.setAction(action);
        return mapper.map(taskService.save(task), TaskResponse.class);
    }

    @PostMapping("login")
    public TaskResponse installInstagram(@RequestBody InstagramLoginTaskRequest commonTaskRequest) {
        Task task = mapper.map(commonTaskRequest, Task.class);
        Action action = new Action();
        //TODO GET ACTION  and all dictionary value FROM DYNAMIC ENUM(enum get values from database)
        action.setId(7L);
        task.setAction(action);
        return mapper.map(taskService.save(task), TaskResponse.class);
    }

    @PostMapping("registration")
    public TaskResponse installInstagram(@RequestBody InstagramRegistrationTaskRequest commonTaskRequest) {
        Task task = mapper.map(commonTaskRequest, Task.class);
        Action action = new Action();
        //TODO GET ACTION  and all dictionary value FROM DYNAMIC ENUM(enum get values from database)
        action.setId(8L);
        task.setAction(action);
        return mapper.map(taskService.save(task), TaskResponse.class);
    }

}
