package com.rttf.rest.controller.task;

import com.rttf.business.model.Action;
import com.rttf.business.model.Task;
import com.rttf.business.service.task.TaskService;
import com.rttf.rest.model.request.task.CommonTaskRequest;
import com.rttf.rest.model.request.task.phone.PhoneWifiTaskRequest;
import com.rttf.rest.model.response.TaskResponse;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.rttf.config.swagger.SwaggerConfiguration.PHONE_TASK_TAG;

@RestController
@RequestMapping("/v1/task/phone")
@AllArgsConstructor
@Api(tags = PHONE_TASK_TAG)
public class PhoneTaskControllerV1 {

    private MapperFacade mapper;

    private TaskService taskService;

//    @PostMapping("/proxy")
//    public TaskResponse setProxy(@RequestBody PhoneProxyTaskRequest phoneProxyTaskRequest) {
//        //TODO refactor
//        Task task = mapper.map(phoneProxyTaskRequest, Task.class);
//        Action action = new Action();
//        //TODO GET ACTION  and all dictionary value FROM DYNAMIC ENUM(enum get values from database)
//        action.setId(4L);
//        task.setAction(action);
//        return mapper.map(taskService.save(task), TaskResponse.class);
//    }

//    @PostMapping("/dump")
//    public TaskResponse setDump(@RequestBody PhoneDumpTaskRequest phoneProxyTaskRequest) {
//        //TODO refactor
//        Task task = mapper.map(phoneProxyTaskRequest, Task.class);
//        Action action = new Action();
//        //TODO GET ACTION  and all dictionary value FROM DYNAMIC ENUM(enum get values from database)
//        action.setId(5L);
//        task.setAction(action);
//        return mapper.map(taskService.save(task), TaskResponse.class);
//    }

    @PostMapping("/wipe")
    public TaskResponse wipe(@RequestBody CommonTaskRequest commonTaskRequest) {
        Task task = mapper.map(commonTaskRequest, Task.class);
        Action action = new Action();
        //TODO GET ACTION  and all dictionary value FROM DYNAMIC ENUM(enum get values from database)
        action.setId(4L);
        task.setAction(action);
        return mapper.map(taskService.save(task), TaskResponse.class);
    }

    @PostMapping("/wifi")
    public TaskResponse wifi(@RequestBody PhoneWifiTaskRequest commonTaskRequest) {
        Task task = mapper.map(commonTaskRequest, Task.class);
        Action action = new Action();
        //TODO GET ACTION  and all dictionary value FROM DYNAMIC ENUM(enum get values from database)
        action.setId(5L);
        task.setAction(action);
        return mapper.map(taskService.save(task), TaskResponse.class);
    }

    @PostMapping("install/instagram")
    public TaskResponse installInstagram(@RequestBody CommonTaskRequest commonTaskRequest) {
        Task task = mapper.map(commonTaskRequest, Task.class);
        Action action = new Action();
        //TODO GET ACTION  and all dictionary value FROM DYNAMIC ENUM(enum get values from database)
        action.setId(6L);
        task.setAction(action);
        return mapper.map(taskService.save(task), TaskResponse.class);
    }

}
