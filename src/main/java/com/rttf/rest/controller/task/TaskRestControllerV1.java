package com.rttf.rest.controller.task;

import com.rttf.business.service.task.TaskService;
import com.rttf.rest.model.response.TaskResponse;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.stream.Collectors;

import static com.rttf.config.swagger.SwaggerConfiguration.TASK_TAG;

@RestController
@RequestMapping("/v1/task")
@AllArgsConstructor
@Api(tags = TASK_TAG)
public class TaskRestControllerV1 {

    private static final String ID = "id";

    private MapperFacade mapper;

    private TaskService taskService;

    @GetMapping("/{id}")
    public TaskResponse findById(@PathVariable(value = ID) Long taskId) {
        return mapper.map(taskService.findById(taskId), TaskResponse.class);
    }

    @GetMapping
    public Collection<TaskResponse> findAll() {
        return taskService.findAll().stream().map(task -> mapper.map(task, TaskResponse.class)).collect(Collectors.toList());
    }

}
