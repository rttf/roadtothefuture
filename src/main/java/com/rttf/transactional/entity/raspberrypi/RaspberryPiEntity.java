package com.rttf.transactional.entity.raspberrypi;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "raspberrypi", schema = "phone_manager")
public class RaspberryPiEntity {
    private Long id;
    private String ip;
    private RaspberryPiStatusEntity status;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ip", nullable = false, unique = true)
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @ManyToOne
    @JoinColumn(name = "status_id", referencedColumnName = "id")
    public RaspberryPiStatusEntity getStatus() {
        return status;
    }

    public void setStatus(RaspberryPiStatusEntity status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RaspberryPiEntity that = (RaspberryPiEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(ip, that.ip) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ip, status);
    }

    @Override
    public String toString() {
        return "RaspberryPiEntity{" +
                "id=" + id +
                ", ip='" + ip + '\'' +
                ", status=" + status +
                '}';
    }
}
