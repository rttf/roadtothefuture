package com.rttf.transactional.entity.task;

import com.rttf.business.model.TaskPayload;
import com.rttf.transactional.entity.action.ActionEntity;
import com.rttf.transactional.entity.phone.PhoneEntity;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "task", schema = "phone_manager")
@TypeDefs({
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
public class TaskEntity {
    private Long id;
    private ActionEntity action;
    private TaskStatusEntity status;
    private PhoneEntity phone;
    private TaskPayload payload;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "action_id", referencedColumnName = "id")
    public ActionEntity getAction() {
        return action;
    }

    public void setAction(ActionEntity action) {
        this.action = action;
    }

    @ManyToOne
    @JoinColumn(name = "status_id", referencedColumnName = "id")
    public TaskStatusEntity getStatus() {
        return status;
    }

    public void setStatus(TaskStatusEntity status) {
        this.status = status;
    }

    @ManyToOne
    @JoinColumn(name = "phone_id", referencedColumnName = "id")
    public PhoneEntity getPhone() {
        return phone;
    }

    public void setPhone(PhoneEntity phone) {
        this.phone = phone;
    }

    @Type(type = "jsonb")
    @Column(name = "payload", columnDefinition = "jsonb")
    public TaskPayload getPayload() {
        return payload;
    }

    public void setPayload(TaskPayload payload) {
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskEntity that = (TaskEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(action, that.action) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, action, status);
    }

    @Override
    public String toString() {
        return "TaskEntity{" +
                "id=" + id +
                ", action=" + action +
                ", status=" + status +
                '}';
    }
}
