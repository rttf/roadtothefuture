package com.rttf.transactional.entity.phone;

import com.rttf.transactional.entity.dump.DumpEntity;
import com.rttf.transactional.entity.raspberrypi.RaspberryPiEntity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "phone", schema = "phone_manager")
public class PhoneEntity {

    private Long id;
    private String name;
    private String proxy;
    private DumpEntity dump;
    private PhoneStatusEntity status;
    private RaspberryPiEntity raspberryPi;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, unique = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "proxy")
    public String getProxy() {
        return proxy;
    }

    public void setProxy(String proxy) {
        this.proxy = proxy;
    }

    @OneToOne
    @JoinColumn(name = "dump_id", referencedColumnName = "id")
    public DumpEntity getDump() {
        return dump;
    }

    public void setDump(DumpEntity dump) {
        this.dump = dump;
    }

    @ManyToOne
    @JoinColumn(name = "status_id", referencedColumnName = "id")
    public PhoneStatusEntity getStatus() {
        return status;
    }

    public void setStatus(PhoneStatusEntity status) {
        this.status = status;
    }

    @ManyToOne
    @JoinColumn(name = "raspberrypi_id", referencedColumnName = "id")
    public RaspberryPiEntity getRaspberryPi() {
        return raspberryPi;
    }

    public void setRaspberryPi(RaspberryPiEntity raspberryPi) {
        this.raspberryPi = raspberryPi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneEntity that = (PhoneEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(raspberryPi, that.raspberryPi);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, proxy, dump, status, raspberryPi);
    }

    @Override
    public String toString() {
        return "PhoneEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", proxy='" + proxy + '\'' +
                ", dump=" + dump +
                ", status=" + status +
                ", raspberryPi=" + raspberryPi +
                '}';
    }
}
