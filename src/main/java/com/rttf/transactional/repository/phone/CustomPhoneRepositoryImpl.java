package com.rttf.transactional.repository.phone;

import com.rttf.transactional.entity.phone.PhoneEntity;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Transactional
public class CustomPhoneRepositoryImpl implements CustomPhoneRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public PhoneEntity merge(PhoneEntity entity) {
        return entityManager.merge(entity);
    }

    public PhoneEntity refresh(PhoneEntity entity) {
        entityManager.refresh(entity);
        return entity;
    }

}
