package com.rttf.transactional.repository.phone;

import com.rttf.transactional.entity.phone.PhoneEntity;

public interface CustomPhoneRepository {

    PhoneEntity merge(PhoneEntity entity);
    PhoneEntity refresh(PhoneEntity entity);

}
