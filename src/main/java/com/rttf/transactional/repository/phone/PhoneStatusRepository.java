package com.rttf.transactional.repository.phone;

import com.rttf.transactional.entity.phone.PhoneStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneStatusRepository extends JpaRepository<PhoneStatusEntity, Long> {
}
