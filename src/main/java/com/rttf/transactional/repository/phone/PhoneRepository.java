package com.rttf.transactional.repository.phone;

import com.rttf.transactional.entity.phone.PhoneEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneRepository extends JpaRepository<PhoneEntity, Long>, CustomPhoneRepository {
}
