package com.rttf.transactional.repository.app;

import com.rttf.transactional.entity.app.AppEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppRepository extends JpaRepository<AppEntity, Long>, CustomAppRepository {
}
