package com.rttf.transactional.repository.task;

import com.rttf.transactional.entity.task.TaskEntity;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Transactional
public class CustomTaskRepositoryImpl implements CustomTaskRepository {

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public TaskEntity merge(TaskEntity entity) {
        return entityManager.merge(entity);
    }

    @Override
    public TaskEntity refresh(TaskEntity entity) {
        entityManager.refresh(entity);
        return entity;
    }
}
