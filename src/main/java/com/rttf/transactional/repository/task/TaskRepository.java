package com.rttf.transactional.repository.task;

import com.rttf.transactional.entity.task.TaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<TaskEntity, Long>, CustomTaskRepository {
}
