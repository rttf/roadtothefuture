package com.rttf.transactional.repository.task;

import com.rttf.transactional.entity.task.TaskEntity;

public interface CustomTaskRepository {
    TaskEntity merge(TaskEntity entity);

    TaskEntity refresh(TaskEntity entity);
}
