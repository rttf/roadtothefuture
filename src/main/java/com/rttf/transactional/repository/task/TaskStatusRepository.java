package com.rttf.transactional.repository.task;

import com.rttf.transactional.entity.task.TaskStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskStatusRepository extends JpaRepository<TaskStatusEntity, Long> {
}
