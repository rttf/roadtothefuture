package com.rttf.transactional.repository.raspberrypi;

import com.rttf.transactional.entity.raspberrypi.RaspberryPiEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RaspberryPiRepository extends JpaRepository<RaspberryPiEntity, Long> {
}
