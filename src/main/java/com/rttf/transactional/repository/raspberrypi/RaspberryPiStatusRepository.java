package com.rttf.transactional.repository.raspberrypi;

import com.rttf.transactional.entity.raspberrypi.RaspberryPiStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RaspberryPiStatusRepository extends JpaRepository<RaspberryPiStatusEntity, Long> {
}
