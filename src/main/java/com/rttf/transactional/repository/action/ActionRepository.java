package com.rttf.transactional.repository.action;

import com.rttf.transactional.entity.action.ActionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActionRepository extends JpaRepository<ActionEntity, Long> {
}
