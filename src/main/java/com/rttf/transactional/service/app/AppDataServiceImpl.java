package com.rttf.transactional.service.app;

import com.rttf.business.model.App;
import com.rttf.transactional.repository.app.AppRepository;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AppDataServiceImpl implements AppDataService {

    private final MapperFacade mapper;

    private final AppRepository appRepository;

    @Override
    public App findById(Long appId) {
        //TODO Add exception
        return appRepository.findById(appId).map(appEntity -> mapper.map(appEntity, App.class)).orElse(null);
    }

    @Override
    public Collection<App> findAll() {
        return appRepository.findAll().stream().map(appEntity -> mapper.map(appEntity, App.class)).collect(Collectors.toList());
    }
}
