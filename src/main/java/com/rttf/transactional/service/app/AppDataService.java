package com.rttf.transactional.service.app;

import com.rttf.business.model.App;

import java.util.Collection;

public interface AppDataService {

    App findById(Long appId);

    Collection<App> findAll();

}
