package com.rttf.transactional.service.rasbperrypi;

import com.rttf.business.model.RaspberryPi;

public interface RaspberryPiDataService {

    RaspberryPi saveRaspberryPi(RaspberryPi raspberryPi);

}
