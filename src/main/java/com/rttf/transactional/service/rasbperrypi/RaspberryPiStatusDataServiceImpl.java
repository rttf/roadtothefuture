package com.rttf.transactional.service.rasbperrypi;

import com.rttf.business.model.RaspberryPiStatus;
import com.rttf.transactional.repository.raspberrypi.RaspberryPiStatusRepository;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RaspberryPiStatusDataServiceImpl implements RaspberryPiStatusDataService {

    private MapperFacade mapper;

    private RaspberryPiStatusRepository raspberryPiStatusRepository;

    @Override
    public RaspberryPiStatus findById(Long statusId) {
        return raspberryPiStatusRepository.findById(statusId).map(raspberryPiStatusEntity -> mapper.map(raspberryPiStatusEntity, RaspberryPiStatus.class)).orElse(null);
    }
}
