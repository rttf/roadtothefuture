package com.rttf.transactional.service.rasbperrypi;

import com.rttf.business.model.RaspberryPiStatus;

public interface RaspberryPiStatusDataService {

    RaspberryPiStatus findById(Long statusId);

}
