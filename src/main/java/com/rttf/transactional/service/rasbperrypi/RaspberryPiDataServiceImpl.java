package com.rttf.transactional.service.rasbperrypi;

import com.rttf.business.model.RaspberryPi;
import com.rttf.transactional.entity.raspberrypi.RaspberryPiEntity;
import com.rttf.transactional.repository.raspberrypi.RaspberryPiRepository;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class RaspberryPiDataServiceImpl implements RaspberryPiDataService {

    private MapperFacade mapper;

    private RaspberryPiRepository raspberryPiRepository;

    @Override
    //TODO add error handling
    public RaspberryPi saveRaspberryPi(RaspberryPi raspberryPi) {
        return Optional.of(mapper.map(raspberryPi, RaspberryPiEntity.class))
                .map(raspberryPiRepository::save)
                .map(savedEntity -> mapper.map(savedEntity, RaspberryPi.class))
                .orElse(null);
    }

}
