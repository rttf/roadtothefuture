package com.rttf.transactional.service.action;

import com.rttf.business.model.Action;

import java.util.Collection;

public interface ActionDataService {

    Action findById(Long id);

    Collection<Action> findAll();

}
