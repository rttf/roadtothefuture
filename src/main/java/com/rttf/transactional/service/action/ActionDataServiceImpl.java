package com.rttf.transactional.service.action;

import com.rttf.business.model.Action;
import com.rttf.transactional.repository.action.ActionRepository;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ActionDataServiceImpl implements ActionDataService {

    private MapperFacade mapper;

    private ActionRepository actionRepository;

    @Override
    public Action findById(Long id) {
        return actionRepository.findById(id).map(actionEntity -> mapper.map(actionEntity, Action.class)).orElse(null);
    }

    @Override
    public Collection<Action> findAll() {
        return actionRepository.findAll().stream().map(actionEntity -> mapper.map(actionEntity, Action.class)).collect(Collectors.toList());
    }
}
