package com.rttf.transactional.service.phone;

import com.rttf.business.model.PhoneStatus;

import java.util.Collection;

public interface PhoneStatusDataService {

    PhoneStatus findById(Long phoneStatusId);

    Collection<PhoneStatus> findAll();

}
