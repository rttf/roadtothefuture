package com.rttf.transactional.service.phone;

import com.rttf.business.model.PhoneStatus;
import com.rttf.transactional.repository.phone.PhoneStatusRepository;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PhoneStatusDataServiceImpl implements PhoneStatusDataService {

    private MapperFacade mapperFacade;

    private PhoneStatusRepository phoneStatusRepository;

    @Override
    public PhoneStatus findById(Long phoneStatusId) {
        return phoneStatusRepository.findById(phoneStatusId).map(phoneStatusEntity -> mapperFacade.map(phoneStatusEntity, PhoneStatus.class)).orElse(null);
    }

    @Override
    public Collection<PhoneStatus> findAll() {
        return phoneStatusRepository.findAll().stream().map(phoneStatusEntity -> mapperFacade.map(phoneStatusEntity, PhoneStatus.class)).collect(Collectors.toList());
    }
}
