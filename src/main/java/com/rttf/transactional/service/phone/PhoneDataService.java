package com.rttf.transactional.service.phone;

import com.rttf.business.model.Phone;

import java.util.Collection;

public interface PhoneDataService {

    Phone findById(Long phoneId);

    Collection<Phone> findAll();

    Phone save(Phone phone);

}
