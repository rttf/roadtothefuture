package com.rttf.transactional.service.phone;

import com.rttf.business.model.Phone;
import com.rttf.transactional.entity.phone.PhoneEntity;
import com.rttf.transactional.repository.phone.PhoneRepository;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PhoneDataServiceImpl implements PhoneDataService {

    private final MapperFacade mapper;

    private final PhoneRepository phoneRepository;

    @Override
    public Phone findById(Long phoneId) {
        return phoneRepository.findById(phoneId).map(phoneEntity -> mapper.map(phoneEntity, Phone.class)).orElse(null);
    }

    @Override
    public Collection<Phone> findAll() {
        return phoneRepository.findAll().stream().map(phoneEntity -> mapper.map(phoneEntity, Phone.class)).collect(Collectors.toList());
    }

    @Override
    public Phone save(Phone phone) {
        return Optional.of(mapper.map(phone, PhoneEntity.class))
                .map(phoneRepository::save)
                .map(savedPhone -> mapper.map(savedPhone, Phone.class)).orElse(null);
    }
}
