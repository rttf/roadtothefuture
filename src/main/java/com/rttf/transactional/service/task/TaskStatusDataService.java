package com.rttf.transactional.service.task;

import com.rttf.business.model.TaskStatus;

import java.util.Collection;

public interface TaskStatusDataService {

    TaskStatus findById(Long taskStatusId);

    Collection<TaskStatus> findAll();

}
