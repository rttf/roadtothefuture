package com.rttf.transactional.service.task;

import com.rttf.business.model.Task;
import com.rttf.transactional.entity.task.TaskEntity;
import com.rttf.transactional.repository.task.TaskRepository;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
public class TaskDataServiceImpl implements TaskDataService {

    private MapperFacade mapper;

    private TaskRepository taskRepository;

    @Override
    public Task findById(Long id) {
        return taskRepository.findById(id).map(taskEntity -> mapper.map(taskEntity, Task.class)).orElse(null);
    }

    @Override
    public Collection<Task> findAll() {
        return taskRepository.findAll().stream().map(taskEntity -> mapper.map(taskEntity, Task.class)).collect(Collectors.toList());

    }

    @Override
    public Task save(Task task) {
        return Optional.of(mapper.map(task, TaskEntity.class))
                .map(taskRepository::save)
                .map(savedEntity -> mapper.map(savedEntity, Task.class)).orElse(null);
    }

    @Override
    public Task update(Task task) {
        return Optional.of(mapper.map(task, TaskEntity.class))
                .map(taskRepository::save)
                .map(savedEntity -> mapper.map(savedEntity, Task.class)).orElse(null);
    }

}
