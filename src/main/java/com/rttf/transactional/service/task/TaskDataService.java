package com.rttf.transactional.service.task;

import com.rttf.business.model.Task;

import java.util.Collection;

public interface TaskDataService {

    Task findById(Long id);

    Collection<Task> findAll();

    Task save(Task task);

    Task update(Task task);
}
