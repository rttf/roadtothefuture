package com.rttf.transactional.service.task;

import com.rttf.business.model.TaskStatus;
import com.rttf.transactional.repository.task.TaskStatusRepository;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TaskStatusDataServiceImpl implements TaskStatusDataService {

    private final MapperFacade mapper;

    private TaskStatusRepository taskStatusRepository;

    @Override
    public TaskStatus findById(Long taskStatusId) {
        return taskStatusRepository.findById(taskStatusId).map(taskStatusEntity -> mapper.map(taskStatusEntity, TaskStatus.class)).orElse(null);
    }

    @Override
    public Collection<TaskStatus> findAll() {
        return taskStatusRepository.findAll().stream().map(taskStatusEntity -> mapper.map(taskStatusEntity, TaskStatus.class)).collect(Collectors.toList());
    }
}
