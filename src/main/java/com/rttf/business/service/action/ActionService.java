package com.rttf.business.service.action;

import com.rttf.business.model.Action;

import java.util.Collection;

public interface ActionService {

    Action findById(Long id);

    Collection<Action> findAll();

}
