package com.rttf.business.service.action;

import com.rttf.business.model.Action;
import com.rttf.transactional.service.action.ActionDataService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@AllArgsConstructor
public class ActionServiceImpl implements ActionService {

    private ActionDataService actionDataService;

    @Override
    public Action findById(Long id) {
        return actionDataService.findById(id);
    }

    @Override
    public Collection<Action> findAll() {
        return actionDataService.findAll();
    }
}
