package com.rttf.business.service.raspberrypi;

import com.rttf.business.model.RaspberryPi;

public interface RaspberryPiService {

    RaspberryPi saveRaspberryPi(RaspberryPi raspberryPi);
}
