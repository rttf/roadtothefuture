package com.rttf.business.service.raspberrypi;

import com.rttf.business.model.Phone;
import com.rttf.business.model.RaspberryPi;
import com.rttf.business.service.phone.PhoneService;
import com.rttf.phonemanager.transactional.service.AdbDeviceDataService;
import com.rttf.transactional.service.rasbperrypi.RaspberryPiDataService;
import com.rttf.transactional.service.rasbperrypi.RaspberryPiStatusDataService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import se.vidstige.jadb.JadbDevice;

import java.util.Collection;


@Service
@AllArgsConstructor
public class RaspberryPiServiceImpl implements RaspberryPiService {

    private RaspberryPiDataService raspberryPiDataService;

    private RaspberryPiStatusDataService raspberryPiStatusDataService;

    private AdbDeviceDataService adbDeviceDataService;

    private PhoneService phoneService;

    @Override
    public RaspberryPi saveRaspberryPi(RaspberryPi raspberryPi) {
        if (raspberryPi.getStatus() == null) {
            raspberryPi.setStatus(raspberryPiStatusDataService.findById(2L));
        }
        RaspberryPi raspberryPiSaved = raspberryPiDataService.saveRaspberryPi(raspberryPi);

        Collection<JadbDevice> jadbDevices = adbDeviceDataService.findAllByRaspberryPiId(raspberryPiSaved.getId());
        jadbDevices.forEach(item -> {
            Phone phone = new Phone();
            phone.setName(item.getSerial());
            phone.setRaspberryPi(raspberryPiSaved);
            phoneService.save(phone);
        });

        return raspberryPiSaved;
    }
}
