package com.rttf.business.service.task;


import com.rttf.business.model.TaskStatus;

import java.util.Collection;

public interface TaskStatusService {

    TaskStatus findById(Long taskStatusId);

    Collection<TaskStatus> findAll();

}
