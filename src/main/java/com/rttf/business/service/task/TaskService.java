package com.rttf.business.service.task;

import com.rttf.business.model.Task;

import java.util.Collection;

public interface TaskService {

    Task findById(Long id);

    Collection<Task> findAll();

    Task save(Task task);

    Task start(Task task);

    Task failed(Task task);

    Task complete(Task task);

}
