package com.rttf.business.service.task;

import com.rttf.business.model.Task;
import com.rttf.business.service.action.ActionService;
import com.rttf.business.service.phone.PhoneService;
import com.rttf.transactional.service.task.TaskDataService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@AllArgsConstructor
public class TaskServiceImpl implements TaskService {

    private TaskDataService taskDataService;

    private PhoneService phoneDataService;

    private ActionService actionDataService;

    private TaskStatusService taskStatusService;

    @Override
    public Task findById(Long id) {
        return taskDataService.findById(id);
    }

    @Override
    public Collection<Task> findAll() {
        return taskDataService.findAll();
    }

    @Override
    public Task save(Task task) {
        //TODO refactor this
        task.setStatus(taskStatusService.findById(1L));
        task.setPhone(phoneDataService.findById(task.getPhone().getId()));
        task.setAction(actionDataService.findById(task.getAction().getId()));
        return taskDataService.save(task);
    }

    @Override
    public Task start(Task task) {
        task.setStatus(taskStatusService.findById(2L));
        return taskDataService.update(task);
    }

    @Override
    public Task failed(Task task) {
        task.setStatus(taskStatusService.findById(4L));
        return taskDataService.update(task);
    }

    @Override
    public Task complete(Task task) {
        //TODO remove magic number
        task.setStatus(taskStatusService.findById(3L));
        return taskDataService.update(task);
    }

}
