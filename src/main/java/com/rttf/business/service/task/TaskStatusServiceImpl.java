package com.rttf.business.service.task;

import com.rttf.business.model.TaskStatus;
import com.rttf.transactional.service.task.TaskStatusDataService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@AllArgsConstructor
public class TaskStatusServiceImpl implements TaskStatusService {

    private TaskStatusDataService taskStatusDataService;

    @Override
    public TaskStatus findById(Long taskStatusId) {
        return taskStatusDataService.findById(taskStatusId);
    }

    @Override
    public Collection<TaskStatus> findAll() {
        return taskStatusDataService.findAll();
    }
}
