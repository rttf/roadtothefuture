package com.rttf.business.service.phone;

import com.rttf.business.model.Phone;
import com.rttf.transactional.service.phone.PhoneDataService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@AllArgsConstructor
public class PhoneServiceImpl implements PhoneService {

    private final PhoneDataService phoneDataService;

    private final PhoneStatusService phoneStatusService;

    @Override
    public Phone findById(Long phoneId) {
        return phoneDataService.findById(phoneId);
    }

    @Override
    public Collection<Phone> findAll() {
        return phoneDataService.findAll();
    }

    @Override
    public Phone save(Phone phone) {
        phone.setStatus(phoneStatusService.findById(1L));
        return phoneDataService.save(phone);
    }

    @Override
    public Phone occupy(Phone phone) {
        phone.setStatus(phoneStatusService.findById(2L));
        return phoneDataService.save(phone);
    }

    @Override
    public Phone realize(Phone phone) {
        phone.setStatus(phoneStatusService.findById(1L));
        return phoneDataService.save(phone);
    }


}
