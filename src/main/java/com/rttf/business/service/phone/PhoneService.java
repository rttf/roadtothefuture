package com.rttf.business.service.phone;

import com.rttf.business.model.Phone;

import java.util.Collection;

public interface PhoneService {

    Phone findById(Long phoneId);

    Collection<Phone> findAll();

    Phone save(Phone phone);

    Phone occupy (Phone phone);

    Phone realize(Phone phone);

}
