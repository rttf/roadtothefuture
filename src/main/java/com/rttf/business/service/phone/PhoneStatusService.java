package com.rttf.business.service.phone;

import com.rttf.business.model.PhoneStatus;

import java.util.Collection;

public interface PhoneStatusService {

    PhoneStatus findById(Long phoneStatusId);

    Collection<PhoneStatus> findAll();

}
