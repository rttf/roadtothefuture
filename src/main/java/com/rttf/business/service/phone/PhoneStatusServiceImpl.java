package com.rttf.business.service.phone;

import com.rttf.business.model.PhoneStatus;
import com.rttf.transactional.service.phone.PhoneStatusDataService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@AllArgsConstructor
public class PhoneStatusServiceImpl implements PhoneStatusService {

    private PhoneStatusDataService phoneStatusDataService;

    @Override
    public PhoneStatus findById(Long phoneStatusId) {
        return phoneStatusDataService.findById(phoneStatusId);
    }

    @Override
    public Collection<PhoneStatus> findAll() {
        return phoneStatusDataService.findAll();
    }
}
