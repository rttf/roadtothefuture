package com.rttf.business.service.app;

import com.rttf.business.model.App;
import com.rttf.transactional.service.app.AppDataService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@AllArgsConstructor
public class AppServiceImpl implements AppService {

    private final AppDataService appDataService;

    @Override
    public App findById(Long appId) {
        return appDataService.findById(appId);
    }

    @Override
    public Collection<App> findAll() {
        return appDataService.findAll();
    }
}
