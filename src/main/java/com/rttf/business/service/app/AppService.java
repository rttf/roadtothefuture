package com.rttf.business.service.app;

import com.rttf.business.model.App;

import java.util.Collection;


public interface AppService {

    App findById(Long appId);

    Collection<App> findAll();

}
