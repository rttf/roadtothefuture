package com.rttf.business.model;

import lombok.Data;

@Data
public class RaspberryPiStatus {
    private Long id;
    private String name;
    private String description;
}
