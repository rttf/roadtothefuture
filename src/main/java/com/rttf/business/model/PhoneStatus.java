package com.rttf.business.model;

import lombok.Data;

@Data
public class PhoneStatus {
    private Long id;
    private String name;
    private String description;
}
