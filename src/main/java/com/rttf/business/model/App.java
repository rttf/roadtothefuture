package com.rttf.business.model;

import lombok.Data;

@Data
public class App {
    private Long id;
    private String name;
    private String description;
}
