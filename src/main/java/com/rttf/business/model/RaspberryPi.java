package com.rttf.business.model;

import lombok.Data;

@Data
public class RaspberryPi {

    private Long id;
    private String ip;
    private RaspberryPiStatus status;

}
