package com.rttf.business.model;

import lombok.Data;

@Data
public class TaskStatus {
    private Long id;
    private String name;
    private String description;
}
