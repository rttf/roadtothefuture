package com.rttf.business.model;

import lombok.Data;

@Data
public class Task {
    private Long id;
    private Phone phone;
    private Action action;
    private TaskStatus status;
    private TaskPayload payload;
}
