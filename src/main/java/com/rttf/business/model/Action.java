package com.rttf.business.model;

import com.rttf.transactional.entity.app.AppEntity;
import lombok.Data;

@Data
public class Action {
    private Long id;
    private String name;
    private String description;
    private AppEntity app;
}
