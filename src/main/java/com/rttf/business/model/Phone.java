package com.rttf.business.model;

import lombok.Data;

@Data
public class Phone {
    private Long id;
    private String name;
    private String proxy;
    private PhoneStatus status;
    private Long dumpId;
    private RaspberryPi raspberryPi;
}
