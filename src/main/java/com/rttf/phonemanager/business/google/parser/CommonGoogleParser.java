package com.rttf.phonemanager.business.google.parser;

import com.rttf.phonemanager.business.helper.Bounds;
import net.sf.saxon.s9api.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;

@Component
@Scope("prototype")
public class CommonGoogleParser {

    public Bounds getCreateAccountButtonCoordinates(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'ow249')]");
    }

    public Bounds getForMyselfCoordinates(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@index,'5')]/node[contains(@index,'1')]");
    }

    public Bounds getFirstNameCoordinates(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'firstName')]");
    }

    public Bounds getLastNameCoordinates(String dump) throws SaxonApiException {
        Bounds firstNameCoordinates = getFirstNameCoordinates(dump);
        Bounds lastNameCoordinates = getBounds(dump, "descendant-or-self::*[contains(@resource-id,'lastName')]");
        return new Bounds(firstNameCoordinates.getX1(), firstNameCoordinates.getX2(), lastNameCoordinates.getY1(), lastNameCoordinates.getY2());
    }

    public Bounds getDayCoordinates(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'day')]");
    }

    public Bounds getCollectNamesNextCoordinates(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'collectNameNext')]");
    }

    //TODO to unitily method
    //Compile regex one time
    private Bounds getBounds(String dump, String regexString) throws SaxonApiException {
        Processor proc = new Processor(false);
        XPathCompiler xpath = proc.newXPathCompiler();
        DocumentBuilder builder = proc.newDocumentBuilder();

        // Load the XML document.
        StringReader reader = new StringReader(dump);
        XdmNode doc = builder.build(new StreamSource(reader));

        // Compile the xpath
        XPathSelector selector = xpath.compile(regexString).load();
        selector.setContextItem(doc);

        // Evaluate the expression.
        XdmValue nodes = selector.evaluateSingle();
        String bounds = "";
        for (XdmItem x : nodes) {
            XdmNode xdmItems = (XdmNode) x;
            bounds = xdmItems.getAttributeValue(QName.fromClarkName("bounds"));
        }
        String[] boundsAfterSplit = bounds.split("\\]\\[");
        boundsAfterSplit[0] = boundsAfterSplit[0].replace("[", "");
        boundsAfterSplit[1] = boundsAfterSplit[1].replace("]", "");
        String[] x1y1 = boundsAfterSplit[0].split(",");
        String[] x2y2 = boundsAfterSplit[1].split(",");

        return new Bounds(Integer.parseInt(x1y1[0]), Integer.parseInt(x2y2[0]), Integer.parseInt(x1y1[1]), Integer.parseInt(x2y2[1]));
    }

}
