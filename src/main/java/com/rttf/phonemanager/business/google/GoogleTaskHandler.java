package com.rttf.phonemanager.business.google;

import com.rttf.business.model.Task;
import com.rttf.phonemanager.business.AppTaskHandler;
import lombok.AllArgsConstructor;
import net.sf.saxon.s9api.SaxonApiException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import se.vidstige.jadb.JadbDevice;
import se.vidstige.jadb.JadbException;

import java.io.IOException;

@Scope("prototype")
@Component
@AllArgsConstructor
public class GoogleTaskHandler implements AppTaskHandler {
    @Override
    public void execute(Task task, JadbDevice device) throws InterruptedException, IOException, JadbException, SaxonApiException {

    }
}
