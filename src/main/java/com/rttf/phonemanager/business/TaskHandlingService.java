package com.rttf.phonemanager.business;

import com.rttf.business.model.Phone;
import com.rttf.business.model.Task;
import com.rttf.business.service.phone.PhoneService;
import com.rttf.business.service.task.TaskService;
import com.rttf.phonemanager.business.instagram.InstagrammTaskHandler;
import com.rttf.phonemanager.business.phone.PhoneTaskHandler;
import com.rttf.phonemanager.transactional.service.AdbDeviceDataService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import se.vidstige.jadb.JadbDevice;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
@Slf4j
public class TaskHandlingService {

    private TaskService taskService;

    private AdbDeviceDataService adbDeviceDataService;

    private PhoneService phoneService;

    private Map<Long, AppTaskHandler> appServices = new HashMap<>();

    private final InstagrammTaskHandler instagrammTaskHandler;

    private final PhoneTaskHandler phoneTaskHandler;

    //TODO check docs
    private static ExecutorService executorService = Executors.newFixedThreadPool(10);

    private static boolean executed = false;

    @PostConstruct
    public void init() {
        appServices.put(1L, instagrammTaskHandler);
        appServices.put(2L, new YoutubeTaskHandler());
        appServices.put(3L, phoneTaskHandler);
    }

    @Scheduled(fixedRate = 10000)
    public void startAvailableTasks() {
        if (!executed) {
            try {
                executed = true;
                Collection<Task> tasks = taskService.findAll().stream().filter(task -> task.getStatus().getId() == 1 && task.getPhone().getStatus().getId() == 1).collect(Collectors.toList());
                if (!tasks.isEmpty()) {
                    tasks.forEach(this::setTaskInProgress);
                    tasks.forEach(task -> task.setPhone(occupyPhone(task.getPhone())));
                    tasks.forEach(task -> executorService.execute(() -> runTask(task)));
                }
            } finally {
                executed = false;
            }
        }
    }

    private Phone occupyPhone(Phone phone) {
        return phoneService.occupy(phone);
    }

    private Phone realizePhone(Phone phone) {
        return phoneService.realize(phone);
    }

    private void setTaskInProgress(Task task) {
        taskService.start(task);
    }

    private void runTask(Task task) {
        try {
            JadbDevice jadbDevice = adbDeviceDataService.findDeviceById(task.getPhone().getId());
            appServices.get(task.getAction().getApp().getId()).execute(task, jadbDevice);
            taskService.complete(task);
        } catch (Exception e) {
            taskService.failed(task);
        } finally {
            task.setPhone(realizePhone(task.getPhone()));
        }
    }

}
