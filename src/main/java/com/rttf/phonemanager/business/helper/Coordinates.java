package com.rttf.phonemanager.business.helper;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Coordinates {
    private int x;
    private int y;
}
