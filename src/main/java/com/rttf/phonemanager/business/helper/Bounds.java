package com.rttf.phonemanager.business.helper;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Bounds {
    private int x1;
    private int x2;
    private int y1;
    private int y2;

    public Coordinates getCoordinates() {
        int dx = x2 - x1;
        int dy = y2 - y1;
        return new Coordinates(x1 + dx / 2, y1 + dy / 2);
    }
}
