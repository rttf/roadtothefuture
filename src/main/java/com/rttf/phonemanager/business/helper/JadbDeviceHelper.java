package com.rttf.phonemanager.business.helper;

import se.vidstige.jadb.JadbDevice;
import se.vidstige.jadb.JadbException;
import se.vidstige.jadb.RemoteFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

//TODO refactor and put strings in constant
public class JadbDeviceHelper {

    private static final int DEFAULT_SLEEP_DURATION = 100;
    private static final int DURATION = 1000;
    private static volatile int counter = 0;

    private JadbDevice device;

    public JadbDeviceHelper(JadbDevice jadbDevice) {
        this.device = jadbDevice;
    }

    public void tap(int x, int y) throws IOException, JadbException, InterruptedException {
        device.executeShell("input tap", String.valueOf(x), String.valueOf(y));
        sleep(DURATION);
    }

    public void tap(Bounds bounds) throws InterruptedException, IOException, JadbException {
        this.tap(bounds.getX1() + ((bounds.getX2()-bounds.getX1())/2), bounds.getY1() + ((bounds.getY2()-bounds.getY1())/2));
    }

    public void swipe(int x1, int y1, int x2, int y2, int duration) throws IOException, JadbException, InterruptedException {
        device.executeShell("input swipe", String.valueOf(x1), String.valueOf(y1), String.valueOf(x2), String.valueOf(y2), String.valueOf(duration));
        sleep(DURATION);
    }

    public void proxyOn(String ip, String port) throws IOException, JadbException {
        String proxy = String.format("%s:%s", ip, port);
        device.executeShell("settings put global http_proxy " + proxy);
    }

    public void proxyOff() throws IOException, JadbException, InterruptedException {
        device.executeShell("settings delete global http_proxy");
        sleep(DURATION);
        device.executeShell("settings delete global global_http_proxy_host");
        sleep(DURATION);
        device.executeShell("settings delete global global_http_proxy_port");
        sleep(DURATION);
        reboot();
    }

    public void reboot() throws IOException, JadbException {
        device.executeShell("reboot");
    }

    public void longTap(int x, int y, int duration) throws IOException, JadbException, InterruptedException {
        device.executeShell("input swipe", String.valueOf(x), String.valueOf(y), String.valueOf(x), String.valueOf(y), String.valueOf(duration));
        sleep(DURATION);
    }

    public void keyEvent(KeyEvent keyEvent) throws IOException, JadbException, InterruptedException {
        device.executeShell("input keyevent", String.valueOf(keyEvent.getValue()));
        sleep(DURATION);
    }

    public void text(String text) throws IOException, JadbException, InterruptedException {
        text = text.replace(" ", "%s");
        device.executeShell("input text", text);
        sleep(DURATION);
    }

    public void startApp(String packageName) throws IOException, JadbException, InterruptedException {
        device.executeShell("monkey -p", packageName, "1");
        sleep(DURATION);
    }

    //TODO checking InputStream for finish result
    public void install(String pathToApp) throws IOException, JadbException {
        InputStream inputStream = device.executeShell("pm install -t -r " + pathToApp);
        System.out.println("asd");
    }

    public void openWifiSettings() throws IOException, JadbException, InterruptedException {
        device.executeShell("am start -a android.intent.action.MAIN -n com.android.settings/.wifi.WifiSettings");
        sleep(DURATION);
    }

    public void rebootRecovery() throws IOException, JadbException, InterruptedException {
        InputStream inputStream = device.executeShell("reboot recovery");
        sleep(26000);
    }

    public void wipeData() throws InterruptedException, IOException, JadbException {
        device.executeShell("recovery --wipe-data");
        sleep(180000);
    }

    public void unlock() throws IOException, JadbException, InterruptedException {
        this.keyEvent(KeyEvent.KEYCODE_HOME);
        sleep(DURATION);
        swipe(50, 600, 60, 200, 100);
//      this.keyEvent(KeyEvent.KEYCODE_MENU);
    }
    //TODO HARD REFACTOR THIS HARDCODE

    public synchronized String dump() throws IOException, JadbException, InterruptedException {
        counter++;
        InputStream inputStream = device.executeShell("uiautomator dump /sdcard/dump" + counter + ".xml");
        sleep(4000);
        File dumpFile = new File("dump" + counter + ".xml");
        device.pull(new RemoteFile("/sdcard/dump" + counter + ".xml"), dumpFile);
        device.executeShell("rm", "/sdcard/dump" + counter + ".xml");
        String dump = readLineByLineJava8(dumpFile);
//        dumpFile.delete();
        return dump;
    }

    private void sleep() throws InterruptedException {
        this.sleep(DEFAULT_SLEEP_DURATION);
    }

    private void sleep(int duration) throws InterruptedException {
        Thread.sleep(duration);
    }

    private String readInputStream(InputStream inputStream) throws IOException {
        StringBuilder textBuilder = new StringBuilder();
        try (Reader reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName(StandardCharsets.UTF_8.name())))) {
            int c = 0;
            while ((c = reader.read()) != -1) {
                textBuilder.append((char) c);
            }
        }
        return textBuilder.toString();
    }

    private String readLineByLineJava8(File file) {
        StringBuilder contentBuilder = new StringBuilder();

        try (Stream<String> stream = Files.lines(Paths.get(file.getPath()), StandardCharsets.UTF_8)) {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return contentBuilder.toString();
    }
}
