package com.rttf.phonemanager.business.helper;

public enum KeyEvent {

   KEYCODE_HOME(3),
   KEYCODE_BACK(4),
   KEYCODE_POWER(26),
   KEYCODE_CLEAR(28),
   KEYCODE_ENTER(66),
   KEYCODE_DEL(67),
   KEYCODE_MENU(82);


   private final int value;

   KeyEvent(final int newValue) {
      value = newValue;
   }

   public int getValue() { return value; }
}
