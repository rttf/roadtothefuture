package com.rttf.phonemanager.business.phone.parser;

import com.rttf.phonemanager.business.helper.Bounds;
import net.sf.saxon.s9api.DocumentBuilder;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XPathCompiler;
import net.sf.saxon.s9api.XPathSelector;
import net.sf.saxon.s9api.XdmItem;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XdmValue;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;

@Component
@Scope("prototype")
public class CommonPhoneParser {

    public Bounds getUrlAreaCoordinates(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.android.chrome:id/url_bar')]");
    }

    public Bounds getLikeButtonCoordinates(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.android.chrome:id/coordinator')]/node/node/node/node[contains(@index,'4')]/node");
    }

    public Bounds getWifiEnableSwitch(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@class,'android.widget.Switch')]");
    }

    public Bounds getStartButton(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.sec.android.app.SecSetupWizard:id/next_btn_text')]");
    }

    public Bounds getNextButton(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.android.settings:id/next_button_text')]");
    }

    public Bounds getTermConfirming(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.sec.android.app.SecSetupWizard:id/terms_confirm')]");
    }

    public Bounds getDateTimeNext(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.google.android.setupwizard:id/setup_wizard_navbar_next')]");
    }

    public Bounds getNoConfirming(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.sec.android.app.SecSetupWizard:id/err_collection_cancel')]");
    }

    public Bounds getButtonOk(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'android:id/button1')]");
    }

    public Bounds getButtonSkip(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'android:id/button2')]");
    }

    public Bounds getRightWifi(String dump, String name) throws SaxonApiException {
        return getBounds(dump, String.format("descendant-or-self::*[contains(@text,'%s')]", name));
    }

    public Bounds getAdditionalParam(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.android.settings:id/wifi_advanced_toggle')]");
    }

    public Bounds getPane(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'android:id/customPanel')]");
    }

    public Bounds getProxy(String dump) throws SaxonApiException{
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.android.settings:id/proxy_settings_fields')]/node[contains(@index,'1')]/node");
    }

    public Bounds getManualProxy(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@class,'android.widget.ListView')]/node[contains(@index,'1')]");
    }

    public Bounds getProxyFieldIp(String dump) throws SaxonApiException{
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.android.settings:id/proxy_hostname')]");
    }

    public Bounds getProxyFieldPort(String dump) throws SaxonApiException{
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.android.settings:id/proxy_fields')]/node[contains(@index,'1')]/node[contains(@index,'1')]");
    }


    private Bounds getBounds(String dump, String regexString) throws SaxonApiException {
        Processor proc = new Processor(false);
        XPathCompiler xpath = proc.newXPathCompiler();
        DocumentBuilder builder = proc.newDocumentBuilder();

        // Load the XML document.
        StringReader reader = new StringReader(dump);
        XdmNode doc = builder.build(new StreamSource(reader));

        // Compile the xpath
        XPathSelector selector = xpath.compile(regexString).load();
        selector.setContextItem(doc);

        // Evaluate the expression.
        XdmValue nodes = selector.evaluateSingle();
        String bounds = "";
        for (XdmItem x : nodes) {
            XdmNode xdmItems = (XdmNode) x;
            bounds = xdmItems.getAttributeValue(QName.fromClarkName("bounds"));
        }
        String[] boundsAfterSplit = bounds.split("\\]\\[");
        boundsAfterSplit[0] = boundsAfterSplit[0].replace("[", "");
        boundsAfterSplit[1] = boundsAfterSplit[1].replace("]", "");
        String[] x1y1 = boundsAfterSplit[0].split(",");
        String[] x2y2 = boundsAfterSplit[1].split(",");
        System.out.println("finish coords");
        return new Bounds(Integer.parseInt(x1y1[0]), Integer.parseInt(x2y2[0]), Integer.parseInt(x1y1[1]), Integer.parseInt(x2y2[1]));
    }
}
