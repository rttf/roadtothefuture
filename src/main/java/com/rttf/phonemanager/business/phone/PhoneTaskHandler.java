package com.rttf.phonemanager.business.phone;

import com.rttf.business.model.Task;
import com.rttf.business.model.TaskPayload;
import com.rttf.phonemanager.business.AppTaskHandler;
import com.rttf.phonemanager.business.helper.Bounds;
import com.rttf.phonemanager.business.helper.JadbDeviceHelper;
import com.rttf.phonemanager.business.helper.KeyEvent;
import com.rttf.phonemanager.business.phone.parser.CommonPhoneParser;
import lombok.AllArgsConstructor;
import net.sf.saxon.s9api.SaxonApiException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import se.vidstige.jadb.JadbDevice;
import se.vidstige.jadb.JadbException;
import se.vidstige.jadb.RemoteFile;

import java.io.File;
import java.io.IOException;

@Component
@Scope("prototype")
@AllArgsConstructor
public class PhoneTaskHandler implements AppTaskHandler {

    private CommonPhoneParser commonPhoneParser;

    @Override
    public void execute(Task task, JadbDevice device) throws InterruptedException, IOException, JadbException, SaxonApiException {
        JadbDeviceHelper jadbDeviceHelper = new JadbDeviceHelper(device);
        if ("Wipe".equals(task.getAction().getName())) {
            wipe(jadbDeviceHelper);
        } else if ("Wifi_proxy".equals(task.getAction().getName())) {
            wifi(jadbDeviceHelper, task.getPayload());
        } else if ("Install_instagram".equals(task.getAction().getName())) {
            //TODO to resources
            device.push(new File("C:/RoadToTheFuture/roadtothefuture/roadtothefuture/Instagram.apk"), new RemoteFile("/sdcard/Instagram.apk"));
            Thread.sleep(20000);
            jadbDeviceHelper.install("/sdcard/Instagram.apk");
            Thread.sleep(80000);
        } else {
            throw new RuntimeException();
        }
    }

    private void wifi(JadbDeviceHelper jadbDeviceHelper, TaskPayload payload) throws IOException, JadbException, InterruptedException, SaxonApiException {
        jadbDeviceHelper.unlock();
        Thread.sleep(2000);
        jadbDeviceHelper.proxyOn(payload.getMap().get("proxyIp"), payload.getMap().get("proxyPort"));
        Thread.sleep(2000);
        jadbDeviceHelper.openWifiSettings();
        Thread.sleep(2000);
        Bounds rightWifi = commonPhoneParser.getRightWifi(jadbDeviceHelper.dump(), payload.getMap().get("name"));
        jadbDeviceHelper.tap(rightWifi.getCoordinates().getX(), rightWifi.getCoordinates().getY());
        Thread.sleep(2000);
        jadbDeviceHelper.text(payload.getMap().get("password"));
        jadbDeviceHelper.keyEvent(KeyEvent.KEYCODE_BACK);
        Thread.sleep(3000);
        Thread.sleep(3000);
//        Bounds additionalParam = commonPhoneParser.getAdditionalParam(jadbDeviceHelper.dump());
//        jadbDeviceHelper.tap(additionalParam.getCoordinates().getX(), additionalParam.getCoordinates().getY());
//        Thread.sleep(2000);
//        Bounds proxy = commonPhoneParser.getProxy(jadbDeviceHelper.dump());
//        jadbDeviceHelper.tap(proxy.getCoordinates().getX(), proxy.getCoordinates().getY());
//        Thread.sleep(2000);
//        Bounds manualProxy = commonPhoneParser.getManualProxy(jadbDeviceHelper.dump());
//        jadbDeviceHelper.tap(manualProxy.getCoordinates().getX(), manualProxy.getCoordinates().getY());
//        Bounds proxyIp = commonPhoneParser.getProxyFieldIp(jadbDeviceHelper.dump());
//        jadbDeviceHelper.tap(proxyIp.getCoordinates().getX(), proxyIp.getCoordinates().getY());
//        Thread.sleep(1000);
//        jadbDeviceHelper.text(payload.getMap().get("ip"));
//        Thread.sleep(1000);
//        jadbDeviceHelper.keyEvent(KeyEvent.KEYCODE_BACK);
//        Thread.sleep(2000);
//
//        Bounds proxyPort = commonPhoneParser.getProxyFieldPort(jadbDeviceHelper.dump());
//        jadbDeviceHelper.tap(proxyPort.getCoordinates().getX(), proxyPort.getCoordinates().getY());
//        Thread.sleep(1000);
//        jadbDeviceHelper.text(payload.getMap().get("port"));
//        Thread.sleep(1000);
//        jadbDeviceHelper.keyEvent(KeyEvent.KEYCODE_BACK);
//        Thread.sleep(2000);
        Bounds buttonConnect = commonPhoneParser.getButtonOk(jadbDeviceHelper.dump());
        jadbDeviceHelper.tap(buttonConnect.getCoordinates().getX(), buttonConnect.getCoordinates().getY());

        //TODO
        //proxy adb shell settings put global http_proxy <ip>:<port>
        //adb shell settings delete global http_proxy
        //adb shell settings delete global global_http_proxy_host
        //adb shell settings delete global global_http_proxy_port
    }

    private void wipe(JadbDeviceHelper jadbDeviceHelper) throws IOException, JadbException, InterruptedException, SaxonApiException {
        jadbDeviceHelper.rebootRecovery();
        jadbDeviceHelper.wipeData();

        skipStartupScreen(jadbDeviceHelper, commonPhoneParser);
    }

    private void skipStartupScreen(JadbDeviceHelper jadbDeviceHelper,
                                   CommonPhoneParser commonPhoneParser) throws SaxonApiException, IOException, JadbException, InterruptedException {
        jadbDeviceHelper.keyEvent(KeyEvent.KEYCODE_HOME);
        Bounds next = commonPhoneParser.getStartButton(jadbDeviceHelper.dump());
        jadbDeviceHelper.tap(next.getCoordinates().getX(), next.getCoordinates().getY());
        Thread.sleep(1000);
        jadbDeviceHelper.tap(next.getCoordinates().getX(), next.getCoordinates().getY());
        Thread.sleep(1000);
//      Bounds wifiSwitch = commonPhoneParser.getNextButton(jadbDeviceHelper.dump());
        jadbDeviceHelper.tap(next.getCoordinates().getX(), next.getCoordinates().getY());
        Thread.sleep(1000);
        Bounds termConfirming = commonPhoneParser.getTermConfirming(jadbDeviceHelper.dump());
        jadbDeviceHelper.tap(termConfirming.getCoordinates().getX(), termConfirming.getCoordinates().getY());
        Thread.sleep(1000);
        Bounds noConfirming = commonPhoneParser.getNoConfirming(jadbDeviceHelper.dump());
        jadbDeviceHelper.tap(noConfirming.getCoordinates().getX(), noConfirming.getCoordinates().getY());
        Thread.sleep(1000);
//      wifiSwitch = commonPhoneParser.getStartButton(jadbDeviceHelper.dump());
        jadbDeviceHelper.tap(next.getCoordinates().getX(), next.getCoordinates().getY());
        Thread.sleep(1000);
//      Bounds next = commonPhoneParser.getDateTimeNext(jadbDeviceHelper.dump());
        jadbDeviceHelper.tap(next.getCoordinates().getX(), next.getCoordinates().getY());
        Thread.sleep(1000);
        jadbDeviceHelper.tap(next.getCoordinates().getX(), next.getCoordinates().getY());
        Thread.sleep(1000);
        jadbDeviceHelper.tap(next.getCoordinates().getX(), next.getCoordinates().getY());
        Thread.sleep(1000);
        Bounds ok = commonPhoneParser.getButtonOk(jadbDeviceHelper.dump());
        jadbDeviceHelper.tap(ok.getCoordinates().getX(), ok.getCoordinates().getY());
        jadbDeviceHelper.tap(next.getCoordinates().getX(), next.getCoordinates().getY());
        Thread.sleep(1000);
        jadbDeviceHelper.tap(next.getCoordinates().getX(), next.getCoordinates().getY());
        Thread.sleep(1000);
        jadbDeviceHelper.tap(next.getCoordinates().getX(), next.getCoordinates().getY());
        Thread.sleep(2000);
        jadbDeviceHelper.tap(next.getCoordinates().getX(), next.getCoordinates().getY());
        Thread.sleep(1000);
        Bounds skip = commonPhoneParser.getButtonSkip(jadbDeviceHelper.dump());
        jadbDeviceHelper.tap(skip.getCoordinates().getX(), skip.getCoordinates().getY());
        Thread.sleep(1000);
        jadbDeviceHelper.tap(next.getCoordinates().getX(), next.getCoordinates().getY());
        Thread.sleep(1000);
    }
}
