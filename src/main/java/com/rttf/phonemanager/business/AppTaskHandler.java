package com.rttf.phonemanager.business;

import com.rttf.business.model.Task;
import net.sf.saxon.s9api.SaxonApiException;
import se.vidstige.jadb.JadbDevice;
import se.vidstige.jadb.JadbException;

import java.io.IOException;

public interface AppTaskHandler {

    void execute(Task task, JadbDevice device) throws InterruptedException, IOException, JadbException, SaxonApiException;

}
