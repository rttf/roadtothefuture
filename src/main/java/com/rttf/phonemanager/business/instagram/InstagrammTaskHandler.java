package com.rttf.phonemanager.business.instagram;

import com.rttf.business.model.Task;
import com.rttf.business.model.TaskPayload;
import com.rttf.phonemanager.business.AppTaskHandler;
import com.rttf.phonemanager.business.helper.Bounds;
import com.rttf.phonemanager.business.helper.JadbDeviceHelper;
import com.rttf.phonemanager.business.instagram.parser.CommonInstagramParser;
import lombok.AllArgsConstructor;
import net.sf.saxon.s9api.SaxonApiException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import se.vidstige.jadb.JadbDevice;
import se.vidstige.jadb.JadbException;

import java.io.IOException;

@Scope("prototype")
@Component
@AllArgsConstructor
public class InstagrammTaskHandler implements AppTaskHandler {

    private CommonInstagramParser commonInstagramParser;

    private InstagramTaskPayloadValidator instagramTaskPayloadValidator;

    @Override
    public void execute(Task task, JadbDevice device) throws InterruptedException, SaxonApiException, JadbException, IOException {
        //TODO ADD ENUM For like as REST layer
        if ("Like".equals(task.getAction().getName())) {
            like(task, device);
        } else if ("Follow".equals(task.getAction().getName())) {
            follow(task, device);
        } else if ("Login".equals(task.getAction().getName())) {
            login(task, device);
        } else if ("Logout".equals(task.getAction().getName())) {
            logout(task, device);
        } else if ("Registration".equals(task.getAction().getName())) {
            registration(task, device);
        } else {
            throw new RuntimeException();
        }
    }

    private void registration(Task task, JadbDevice device) throws InterruptedException, IOException, JadbException, SaxonApiException {
        JadbDeviceHelper jadbDeviceHelper = new JadbDeviceHelper(device);
        jadbDeviceHelper.unlock();
        Thread.sleep(1000);
        jadbDeviceHelper.startApp("com.instagram.android");
        Thread.sleep(10000);
        jadbDeviceHelper.tap(commonInstagramParser.getRegisterButton(jadbDeviceHelper.dump()));
        Thread.sleep(2000);
        jadbDeviceHelper.tap(commonInstagramParser.getEmailTab(jadbDeviceHelper.dump()));
        Thread.sleep(2000);
        jadbDeviceHelper.text(task.getPayload().getMap().get("login"));
        Thread.sleep(2000);
        jadbDeviceHelper.tap(commonInstagramParser.getNextRightButton(jadbDeviceHelper.dump()));
        Thread.sleep(2000);
        jadbDeviceHelper.tap(commonInstagramParser.getFullName(jadbDeviceHelper.dump()));
        Thread.sleep(2000);
        jadbDeviceHelper.text(task.getPayload().getMap().get("name"));
        Thread.sleep(2000);
        jadbDeviceHelper.tap(commonInstagramParser.getPasswordField(jadbDeviceHelper.dump()));
        Thread.sleep(2000);
        jadbDeviceHelper.text(task.getPayload().getMap().get("password"));
        Thread.sleep(2000);
        jadbDeviceHelper.tap(commonInstagramParser.getContinueWithoutCI(jadbDeviceHelper.dump()));
        Thread.sleep(2000);
        jadbDeviceHelper.tap(commonInstagramParser.getEnterInInstagrammButton(jadbDeviceHelper.dump()));
        Thread.sleep(2000);
        jadbDeviceHelper.tap(commonInstagramParser.getSkipButton(jadbDeviceHelper.dump()));
        Thread.sleep(2000);
        jadbDeviceHelper.tap(commonInstagramParser.getNegativeButton(jadbDeviceHelper.dump()));
        Thread.sleep(2000);
        jadbDeviceHelper.tap(commonInstagramParser.getSkipButton(jadbDeviceHelper.dump()));
        Thread.sleep(2000);
    }

    private void logout(Task task, JadbDevice device) {

    }

    private void login(Task task, JadbDevice device) throws InterruptedException, IOException, JadbException, SaxonApiException {
        TaskPayload payload = task.getPayload();
        instagramTaskPayloadValidator.validateLikePayload(payload);
        JadbDeviceHelper jadbDeviceHelper = new JadbDeviceHelper(device);
        jadbDeviceHelper.startApp("com.instagram.android");
        try {
            Bounds enterButton = commonInstagramParser.getEnterButton(jadbDeviceHelper.dump());
            jadbDeviceHelper.tap(enterButton.getCoordinates().getX(), enterButton.getCoordinates().getY());
            Bounds login = commonInstagramParser.getLoginField(jadbDeviceHelper.dump());
            jadbDeviceHelper.tap(login.getCoordinates().getX(), login.getCoordinates().getY());
            jadbDeviceHelper.text("earth_pretty_world");
        } catch (Exception e) {
            //TODO add login or something else
        }

        Bounds password = commonInstagramParser.getPasswordField(jadbDeviceHelper.dump());
        jadbDeviceHelper.tap(password.getCoordinates().getX(), password.getCoordinates().getY());
        jadbDeviceHelper.text("z80x90z80x90");

        Bounds enter = commonInstagramParser.getEnterInInstagrammButton(jadbDeviceHelper.dump());
        jadbDeviceHelper.tap(enter.getCoordinates().getX(), enter.getCoordinates().getY());

    }

    private void follow(Task task, JadbDevice device) {

    }

    private void like(Task task, JadbDevice device) {

    }

}
