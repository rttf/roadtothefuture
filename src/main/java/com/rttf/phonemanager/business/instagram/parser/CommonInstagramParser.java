package com.rttf.phonemanager.business.instagram.parser;

import com.rttf.phonemanager.business.helper.Bounds;
import net.sf.saxon.s9api.DocumentBuilder;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XPathCompiler;
import net.sf.saxon.s9api.XPathSelector;
import net.sf.saxon.s9api.XdmItem;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XdmValue;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;

@Component
@Scope("prototype")
public class CommonInstagramParser {

    public Bounds getUrlAreaCoordinates(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.android.chrome:id/url_bar')]");
    }

    public Bounds getLikeButtonCoordinates(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.android.chrome:id/coordinator')]/node/node/node/node[contains(@index,'4')]/node");
    }

    public Bounds getEnterButton(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.instagram.android:id/log_in_button')]");
    }

    public Bounds getNextRightButton(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.instagram.android:id/right_tab_next_button')]");
    }


    public Bounds getLoginField(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.instagram.android:id/login_username')]");
    }

    public Bounds getPasswordField(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.instagram.android:id/password')]");
    }

    public Bounds getFullName(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.instagram.android:id/full_name_inline_error')]/node");
    }

    public Bounds getContinueWithoutCI(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.instagram.android:id/continue_without_ci')]");
    }

    //also next in sign up
    public Bounds getEnterInInstagrammButton(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.instagram.android:id/button_text')]");
    }

    public Bounds getRegisterButton(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.instagram.android:id/sign_up_with_email_or_phone')]");
    }

    public Bounds getEmailTab(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.instagram.android:id/right_tab')]");
    }

    public Bounds getSkipButton(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.instagram.android:id/skip_button')]");
    }

    public Bounds getNegativeButton(String dump) throws SaxonApiException {
        return getBounds(dump, "descendant-or-self::*[contains(@resource-id,'com.instagram.android:id/negative_button')]");
    }

    private Bounds getBounds(String dump, String regexString) throws SaxonApiException {
        Processor proc = new Processor(false);
        XPathCompiler xpath = proc.newXPathCompiler();
        DocumentBuilder builder = proc.newDocumentBuilder();

        // Load the XML document.
        StringReader reader = new StringReader(dump);
        XdmNode doc = builder.build(new StreamSource(reader));

        // Compile the xpath
        XPathSelector selector = xpath.compile(regexString).load();
        selector.setContextItem(doc);

        // Evaluate the expression.
        XdmValue nodes = selector.evaluateSingle();
        String bounds = "";
        for (XdmItem x : nodes) {
            XdmNode xdmItems = (XdmNode) x;
            bounds = xdmItems.getAttributeValue(QName.fromClarkName("bounds"));
        }
        String[] boundsAfterSplit = bounds.split("\\]\\[");
        boundsAfterSplit[0] = boundsAfterSplit[0].replace("[", "");
        boundsAfterSplit[1] = boundsAfterSplit[1].replace("]", "");
        String[] x1y1 = boundsAfterSplit[0].split(",");
        String[] x2y2 = boundsAfterSplit[1].split(",");

        return new Bounds(Integer.parseInt(x1y1[0]), Integer.parseInt(x2y2[0]), Integer.parseInt(x1y1[1]), Integer.parseInt(x2y2[1]));
    }
}
