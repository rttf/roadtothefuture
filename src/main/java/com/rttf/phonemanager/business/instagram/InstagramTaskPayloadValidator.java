package com.rttf.phonemanager.business.instagram;

import com.rttf.business.model.TaskPayload;
import org.springframework.stereotype.Component;

@Component
public class InstagramTaskPayloadValidator {

    //TODO refactor to common validators and and true Exception
    void validateLikePayload(TaskPayload payload) {
        if (!payload.getMap().containsKey("login") || payload.getMap().containsKey("password")) {
            throw new RuntimeException();
        }
    }
}
