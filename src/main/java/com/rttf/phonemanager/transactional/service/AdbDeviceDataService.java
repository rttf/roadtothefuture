package com.rttf.phonemanager.transactional.service;

import se.vidstige.jadb.JadbDevice;

import java.util.Collection;

public interface AdbDeviceDataService {

    JadbDevice findDeviceById(Long id);

    JadbDevice findDevice(String deviceId);

    JadbDevice findDevice(Long raspberryPiId, String deviceId);

    Collection<JadbDevice> findAllByRaspberryPiId(Long raspberryPiId);
}
