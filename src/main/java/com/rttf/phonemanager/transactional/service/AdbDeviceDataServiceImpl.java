package com.rttf.phonemanager.transactional.service;

import com.rttf.business.model.Phone;
import com.rttf.phonemanager.business.helper.JadbDeviceHelper;
import com.rttf.transactional.entity.raspberrypi.RaspberryPiEntity;
import com.rttf.transactional.repository.raspberrypi.RaspberryPiRepository;
import com.rttf.transactional.service.phone.PhoneDataService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import se.vidstige.jadb.JadbConnection;
import se.vidstige.jadb.JadbDevice;
import se.vidstige.jadb.JadbException;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AdbDeviceDataServiceImpl implements AdbDeviceDataService {

    private RaspberryPiRepository raspberryPiRepository;
    private PhoneDataService phoneDataService;
    //TODO check JadbConnection

    @Override
    public JadbDevice findDeviceById(Long id) {
        Phone phone = phoneDataService.findById(id);
        return findDevice(phone.getRaspberryPi().getId(), phone.getName());
    }

    //TODO split into two layers Business and Transactional
    @Override
    public JadbDevice findDevice(String deviceId) {
        return null;
    }

    @Override
    public JadbDevice findDevice(Long raspberryPiId, String deviceId) {

        Optional<JadbDevice> jadbDevice = null;
        Optional<RaspberryPiEntity> raspberryPiEntityOptional = raspberryPiRepository.findById(raspberryPiId);
        if (raspberryPiEntityOptional.isPresent()) {
            RaspberryPiEntity raspberryPiEntity = raspberryPiEntityOptional.get();
            JadbConnection jadbConnection = new JadbConnection(raspberryPiEntity.getIp(), 5037);
//            JadbConnection jadbConnection = new JadbConnection(raspberryPiEntity.getIp(), 5037);
            try {
                jadbDevice = jadbConnection.getDevices().stream().filter(device -> deviceId.equals(device.getSerial())).findFirst();
            } catch (IOException | JadbException e) {
                e.printStackTrace();
            }
        }
        if (jadbDevice != null && jadbDevice.isPresent()) {
            return jadbDevice.get();
        } else {
            throw new RuntimeException();
        }
    }

    @Override
    public Collection<JadbDevice> findAllByRaspberryPiId(Long raspberryPiId) {
        Collection<JadbDevice> jadbDevices = Collections.emptyList();
        Optional<RaspberryPiEntity> raspberryPiEntityOptional = raspberryPiRepository.findById(raspberryPiId);
        if (raspberryPiEntityOptional.isPresent()) {
            RaspberryPiEntity raspberryPiEntity = raspberryPiEntityOptional.get();
            JadbConnection jadbConnection = new JadbConnection(raspberryPiEntity.getIp(), 5037);
            try {
                jadbDevices = jadbConnection.getDevices();
            } catch (IOException | JadbException e) {
                e.printStackTrace();
            }
        }
        return jadbDevices;
    }

}
