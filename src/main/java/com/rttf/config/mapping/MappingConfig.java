package com.rttf.config.mapping;

import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFactory;
import net.rakugakibox.spring.boot.orika.OrikaMapperFactoryConfigurer;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@AllArgsConstructor
public class MappingConfig implements OrikaMapperFactoryConfigurer {

    private final List<Mapper> mappers;

    @Override
    public void configure(MapperFactory orikaMapperFactory) {
        mappers.forEach(mapper -> mapper.configure(orikaMapperFactory));
    }
}
