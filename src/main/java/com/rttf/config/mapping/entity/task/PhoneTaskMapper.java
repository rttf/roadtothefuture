package com.rttf.config.mapping.entity.task;

import com.rttf.business.model.Task;
import com.rttf.config.mapping.Mapper;
import com.rttf.rest.model.request.task.phone.PhoneDumpTaskRequest;
import com.rttf.rest.model.request.task.phone.PhoneProxyTaskRequest;
import com.rttf.rest.model.request.task.phone.PhoneWifiTaskRequest;
import ma.glasnost.orika.MapperFactory;
import org.springframework.stereotype.Component;

@Component
public class PhoneTaskMapper implements Mapper {
    @Override
    public void configure(MapperFactory mapperFactory) {
        mapperFactory
                .classMap(PhoneDumpTaskRequest.class, Task.class)
                .field("dump", "payload.map['dump']")
                .register();

        mapperFactory
                .classMap(PhoneProxyTaskRequest.class, Task.class)
                .field("proxy", "payload.map['proxy']")
                .register();

        mapperFactory
                .classMap(PhoneWifiTaskRequest.class, Task.class)
                .field("name", "payload.map['name']")
                .field("password", "payload.map['password']")
                .field("proxyIp", "payload.map['proxyIp']")
                .field("proxyPort", "payload.map['proxyPort']")
                .register();
    }
}
