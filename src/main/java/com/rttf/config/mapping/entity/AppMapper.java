package com.rttf.config.mapping.entity;

import com.rttf.business.model.App;
import com.rttf.config.mapping.Mapper;
import com.rttf.rest.model.response.AppResponse;
import com.rttf.transactional.entity.app.AppEntity;
import ma.glasnost.orika.MapperFactory;
import org.springframework.stereotype.Component;

@Component
public class AppMapper implements Mapper {
    @Override
    public void configure(MapperFactory mapperFactory) {
        mapperFactory
                .classMap(AppEntity.class, App.class)
                .byDefault()
                .register();

        mapperFactory
                .classMap(App.class, AppResponse.class)
                .byDefault()
                .register();
    }
}
