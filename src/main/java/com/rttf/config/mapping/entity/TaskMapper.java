package com.rttf.config.mapping.entity;

import com.rttf.business.model.Task;
import com.rttf.config.mapping.Mapper;
import com.rttf.rest.model.request.UtilityTaskCreateRequest;
import com.rttf.rest.model.request.task.CommonTaskRequest;
import com.rttf.rest.model.response.TaskResponse;
import com.rttf.transactional.entity.task.TaskEntity;
import ma.glasnost.orika.MapperFactory;
import org.springframework.stereotype.Component;

@Component
public class TaskMapper implements Mapper {

    @Override
    public void configure(MapperFactory mapperFactory) {

        mapperFactory
                .classMap(Task.class, TaskEntity.class)
                .byDefault()
                .register();

        mapperFactory
                .classMap(TaskEntity.class, Task.class)
                .byDefault()
                .register();

        mapperFactory
                .classMap(Task.class, TaskResponse.class)
                .field("payload.map", "payload")
                .field("status.name", "status")
                .byDefault()
                .register();

        mapperFactory
                .classMap(UtilityTaskCreateRequest.class, Task.class)
                .field("phoneId", "phone.id")
                .field("actionId", "action.id")
                .byDefault()
                .register();

        mapperFactory
                .classMap(CommonTaskRequest.class, Task.class)
                .field("phoneId", "phone.id")
                .byDefault()
                .register();
    }

}
