package com.rttf.config.mapping.entity;

import com.rttf.business.model.PhoneStatus;
import com.rttf.config.mapping.Mapper;
import com.rttf.rest.model.response.PhoneStatusResponse;
import com.rttf.transactional.entity.phone.PhoneStatusEntity;
import ma.glasnost.orika.MapperFactory;
import org.springframework.stereotype.Component;

@Component
public class PhoneStatusMapper implements Mapper {

    @Override
    public void configure(MapperFactory mapperFactory) {
        mapperFactory
                .classMap(PhoneStatusEntity.class, PhoneStatus.class)
                .byDefault()
                .register();

        mapperFactory
                .classMap(PhoneStatus.class, PhoneStatusResponse.class)
                .byDefault()
                .register();
    }

}
