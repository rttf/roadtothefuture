package com.rttf.config.mapping.entity;

import com.rttf.business.model.TaskStatus;
import com.rttf.config.mapping.Mapper;
import com.rttf.rest.model.response.TaskStatusResponse;
import com.rttf.transactional.entity.task.TaskStatusEntity;
import ma.glasnost.orika.MapperFactory;
import org.springframework.stereotype.Component;

@Component
public class TaskStatusMapper implements Mapper {

    @Override
    public void configure(MapperFactory mapperFactory) {
        mapperFactory
                .classMap(TaskStatusEntity.class, TaskStatus.class)
                .byDefault()
                .register();

        mapperFactory
                .classMap(TaskStatus.class, TaskStatusResponse.class)
                .byDefault()
                .register();
    }

}
