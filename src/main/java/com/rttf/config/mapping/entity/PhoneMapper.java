package com.rttf.config.mapping.entity;

import com.rttf.business.model.Phone;
import com.rttf.config.mapping.Mapper;
import com.rttf.rest.model.response.PhoneResponse;
import com.rttf.transactional.entity.phone.PhoneEntity;
import ma.glasnost.orika.MapperFactory;
import org.springframework.stereotype.Component;

@Component
public class PhoneMapper implements Mapper {

    @Override
    public void configure(MapperFactory mapperFactory) {
        mapperFactory
                .classMap(PhoneEntity.class, Phone.class)
                .field("dump.id", "dumpId")
                .byDefault()
                .register();

        mapperFactory
                .classMap(Phone.class, PhoneResponse.class)
                .field("status.name", "status")
                .byDefault()
                .register();
    }

}
