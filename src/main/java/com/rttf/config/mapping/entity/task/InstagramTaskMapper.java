package com.rttf.config.mapping.entity.task;

import com.rttf.business.model.Task;
import com.rttf.config.mapping.Mapper;
import com.rttf.rest.model.request.task.instagram.InstagramFollowTaskRequest;
import com.rttf.rest.model.request.task.instagram.InstagramLikeTaskRequest;
import com.rttf.rest.model.request.task.instagram.InstagramLoginTaskRequest;
import com.rttf.rest.model.request.task.instagram.InstagramRegistrationTaskRequest;
import ma.glasnost.orika.MapperFactory;
import org.springframework.stereotype.Component;

@Component
public class InstagramTaskMapper implements Mapper {
    @Override
    public void configure(MapperFactory mapperFactory) {

        mapperFactory
                .classMap(InstagramLikeTaskRequest.class, Task.class)
                .field("postUrl", "payload.map['postUrl']")
                .register();

        mapperFactory
                .classMap(InstagramFollowTaskRequest.class, Task.class)
                .field("profile", "payload.map['profile']")
                .register();

        mapperFactory
                .classMap(InstagramLoginTaskRequest.class, Task.class)
                .field("login", "payload.map['login']")
                .field("password", "payload.map['password']")
                .register();

        mapperFactory
                .classMap(InstagramRegistrationTaskRequest.class, Task.class)
                .field("login", "payload.map['login']")
                .field("password", "payload.map['password']")
                .field("name", "payload.map['name']")
                .register();

    }
}
