package com.rttf.config.mapping;

import ma.glasnost.orika.MapperFactory;

public interface Mapper {

    void configure(MapperFactory mapperFactory);

}
