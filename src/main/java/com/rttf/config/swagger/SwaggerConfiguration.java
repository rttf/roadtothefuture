package com.rttf.config.swagger;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.PropertyResolver;
import org.springframework.util.StringUtils;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;

import static java.util.Optional.ofNullable;

@Configuration
@EnableSwagger2
@RequiredArgsConstructor
@ConditionalOnProperty("swagger.enabled")
public class SwaggerConfiguration {

    public static final String APP_TAG = "app_resource";
    public static final String PHONE_TAG = "phone_resource";
    public static final String PHONE_STATUS_TAG = "phone_status_resource";
    public static final String TASK_STATUS_TAG = "task_status_resource";
    public static final String ACTION_STATUS_TAG = "action_resource";
    public static final String RASPBERRY_PI_TAG = "raspberrypi_resource";
    public static final String TASK_TAG = "task_resource";
    public static final String INSTAGRAM_TASK_TAG = "instagram_task_resource";
    public static final String PHONE_TASK_TAG = "phone_task_resource";

    private final PropertyResolver propertyResolver;

    @Bean
    public Docket apiDocket() {

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.rttf.rest.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(buildApiInfo())
                .tags(
                        new Tag(APP_TAG, "App Services API"),
                        new Tag(PHONE_TAG, "Phone API"),
                        new Tag(PHONE_STATUS_TAG, "Phone status API"),
                        new Tag(TASK_STATUS_TAG, "Task status API"),
                        new Tag(TASK_TAG, "Task API"),
                        new Tag(RASPBERRY_PI_TAG, "Raspberry Pi API"),
                        new Tag(INSTAGRAM_TASK_TAG, "Instagram task API"),
                        new Tag(PHONE_TASK_TAG, "Phone task API"),
                        new Tag(RASPBERRY_PI_TAG, "Action API")
                );
    }

    private ApiInfo buildApiInfo() {
        String year = Integer.toString(LocalDate.now().getYear());
        String description = getDescription();
        String version = getVersion();

        return new ApiInfoBuilder()
                .title(propertyResolver.getProperty("spring.application.name"))
                .description(description)
                .version(version)
                .license(String.format("© %s RTTF Systems, Inc. All Rights Reserved.", year))
                .licenseUrl("#")
                .build();
    }

    private String getDescription() {
        String description = propertyResolver.getProperty("app.api.description");
        return !StringUtils.isEmpty(description) ? description : "ALL API";
    }

    private String getVersion() {
        return ofNullable(propertyResolver.getProperty("app.api.version")).orElse("n/a");
    }

}
