package com.rttf.config.repository;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EntityScan(basePackages = {"com.rttf.transactional.entity"})
@EnableJpaRepositories(basePackages = {"com.rttf.transactional.repository"})
public class JpaConfig {
}
