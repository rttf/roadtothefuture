package com.rttf.config.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
@Scope("prototype")
//TODO add configuration for logging
public class LoggingAspect {

}
